# Sandelinos' Windows 10 Setup Script

I'm not a fan of Windows 10 but I still sometimes need to use it so I wrote
this script to automatically configure new installations to be a little bit
less awful.

## Usage

1. Install windows 10
2. After rebooting and landing in the OOBE, press `CTRL`+`SHIFT`+`F3` to enter
   Audit mode
3. In audit mode insert a flash drive with the script on it
4. Open PowerShell as Administrator
5. Run `Set-ExecutionPolicy Unrestricted`
6. Navigate to where the script is stored and run it.

        > D:
        > & '.\Setup.ps1'

7. Run sysprep (the window should already be open when entering Audit mode) and
   reboot

## What it does

* Uninstalls a bunch of bloatware
    - Cortana
    - Weather
    - Get Help
    - Tips
    - 3D Viewer
    - Office
    - Microsoft Solitaire Collection
    - Mixed Reality Portal
    - Paint 3D
    - OneNote
    - Skype
    - Store Purchase App
    - Alarms & Clock
    - Feedback Hub
    - Maps
    - Voice Recorder
    - Xbox
    - Xbox Game Bar
    - Other Xbox related stuff
    - Your Phone
    - Groove Music
    - Films & TV
    - Spotify
* Disables Advertising ID
* Disables Bing in Windows search
* Stops windows from silently installing more bloatware
* Disables Wi-Fi Sense, 
* Disables telemetry (Win10 Pro only)
* Disables Location service
* Disables Diagnostics Tracking
* Disables "News and Interests"
* Disables "Meet Now" taskbar button
* Removes Edge's desktop shortcut and prevents it from automatically returning
* Disables Input Personalization
* Sets default settings for new users in `C:\Users\Default\NTUSER.DAT`
    - Stops windows from silently installing bloatware
    - Disables Game Bar
    - Disables Bing Search
    - Sets explorer to show file extensions by default
    - Stops OneDrive from automatically being installed for every new user
* Cleans up the Start menu and taskbar
* Installs Firefox and uBlock origin
* Sets Firefox as the default web browser
