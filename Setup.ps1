function Debloat {
	$BloatWare = @(
		"Microsoft.549981C3F5F10"	# Cortana
		"Microsoft.BingWeather"		# Weather
		"Microsoft.GetHelp"		# Get Help
		"Microsoft.Getstarted"		# Tips
		"Microsoft.Microsoft3DViewer"	# 3D Viewer
		"Microsoft.MicrosoftOfficeHub"	# Office
		"Microsoft.MicrosoftSolitaireCollection" # Microsoft Solitaire Collection
		"Microsoft.MixedReality.Portal"	# Mixed Reality Portal
		"Microsoft.MSPaint"		# Paint 3D
		"Microsoft.Office.OneNote"	# OneNote
		"Microsoft.Paint3D"		# Paint 3D (old package name?)
		#"Microsoft.People"		# People
		#"Microsoft.ScreenSketch"	# Snip & Sketch
		"Microsoft.SkypeApp"		# Skype
		"Microsoft.StorePurchaseApp"
		"Microsoft.WindowsAlarms"	# Alarms & Clock
		"Microsoft.WindowsFeedbackHub"	# Feedback Hub
		"Microsoft.WindowsMaps"		# Maps
		"Microsoft.WindowsSoundRecorder"# Voice Recorder
		"Microsoft.XboxApp"		# Xbox
		"Microsoft.XboxGamingOverlay"	# Xbox Game Bar
		"Microsoft.XboxGameOverlay"
		"Microsoft.XboxIdentityProvider"
		"Microsoft.XboxSpeechToTextOverlay"
		"Microsoft.Xbox.TCUI"
		"Microsoft.YourPhone"		# Your Phone
		"Microsoft.ZuneMusic"		# Groove Music
		"Microsoft.ZuneVideo"		# Films & TV
		"SpotifyAB.SpotifyMusic"	# Spotify
	)
	foreach ($App in $Bloatware) {
		Write-Output "Removing $App"
		Get-AppxPackage -Name $App | Remove-AppxPackage
		Get-AppxProvisionedPackage -Online | Where-Object DisplayName -like $App | Remove-AppxProvisionedPackage -Online
	}

	Write-Output "Removing OneDrive"
	Start-Process -Wait -FilePath "$env:systemroot\SysWOW64\OneDriveSetup.exe" -ArgumentList "/uninstall"
}

function Set-Registry {
	param (
		[string]$Path,
		[string]$Name,
		$Value
	)

	if ( !( Test-Path $Path ) ) {
		New-Item $Path
	}
	Set-ItemProperty -Path $Path -Name $Name -Value $Value
}

function Privacy {
	# Disabled because these keys can't be changed by a script
	#Write-Output "Disabling Defender sample submission and enabling tamperprotection"
	#Set-Registry -Path "HKLM:\Software\Microsoft\Windows Defender\Spynet" -Name SubmitSamplesConsent -Value 0
	#Set-Registry -Path "HKLM:\Software\Microsoft\Windows Defender\Features" -Name TamperProtection -Value 5

	Write-Output "Disabling Advertising ID"
	Set-Registry -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\AdvertisingInfo" -Name Enabled -Value 0

	Write-Output "Disabling Bing in Windows Search"
	Set-Registry -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\Windows Search" -Name DisableWebSearch -Value 1

	# Disabled because we uninstall cortana
	#Write-Output "Disabling Cortana in Windows Search"
	#Set-Registry -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\Windows Search" -Name AllowCortana -Value 0

	Write-Output "Adding Registry key to prevent bloatware apps from returning"
	Set-Registry -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\CloudContent" -Name DisableWindowsConsumerFeatures -Value 1

	Write-Output "Disabling Wi-Fi Sense"
	Set-Registry -Path "HKLM:\SOFTWARE\Microsoft\PolicyManager\default\WiFi\AllowWiFiHotSpotReporting" -Name Value -Value 0
	Set-Registry -Path "HKLM:\SOFTWARE\Microsoft\PolicyManager\default\WiFi\AllowAutoConnectToWiFiSenseHotspots" -Name Value -Value 0
	Set-Registry -Path "HKLM:\SOFTWARE\Microsoft\WcmSvc\wifinetworkmanager\config" -Name AutoConnectAllowedOEM -Value 0

	# Doesn't work on Home edition, AllowTelemetry will be automatically reset to 1 ("Required Diagnostic Data") by windows.
	Write-Output "Turning off Data Collection"
	Set-Registry -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\DataCollection" -Name AllowTelemetry -Value 0
	Set-Registry -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\DataCollection" -Name AllowTelemetry -Value 0
	Set-Registry -Path "HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Policies\DataCollection" -Name AllowTelemetry -Value 0

	Write-Output "Disabling Location service"
	Set-Registry -Path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Sensor\Overrides\{BFA794E4-F964-4FDB-90F6-51056BFE4B44}" -Name SensorPermissionState -Value 0
	Set-Registry -Path "HKLM:\SYSTEM\CurrentControlSet\Services\lfsvc\Service\Configuration" -Name Status -Value 0

	Write-Output "Disabling Diagnostics Tracking Service on startup"
	Stop-Service "DiagTrack"
	Set-Service "DiagTrack" -StartupType Disabled # ok

	Write-Output "Disabling News and Interests"
	Set-Registry -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\Windows Feeds" -Name EnableFeeds -Value 0

	Write-Output "Disabling Meet Now taskbar icon"
	Set-Registry -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer" -Name HideSCAMeetNow -Value 1

	Write-Output "Getting rid of Edge desktop icon"
	Remove-Item 'C:\Users\Public\Desktop\Microsoft Edge.lnk'
	Set-Registry -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer" -Name DisableEdgeDesktopShortcutCreation -Value 1
	Set-Registry -Path "HKLM:\SOFTWARE\Policies\Microsoft\EdgeUpdate" -Name CreateDesktopShortcutDefault -Value 0

	Write-Output "Disabling Input Personalization"
	Set-Registry -Path "HKLM:\SOFTWARE\Microsoft\Personalization\Settings" -Name AcceptedPrivacyPolicy -Value 0
	Set-Registry -Path "HKLM:\SOFTWARE\Microsoft\InputPersonalization" -Name RestrictImplicitTextCollection -Value 1
	Set-Registry -Path "HKLM:\SOFTWARE\Microsoft\InputPersonalization" -Name RestrictImplicitInkCollection -Value 1
	Set-Registry -Path "HKLM:\SOFTWARE\Microsoft\InputPersonalization\TrainedDataStore" -Name HarvestContacts -Value 0

	# Loads the template user's NTUSER.DAT to the registry to edit it.
	reg load HKU\Default_User C:\Users\Default\NTUSER.DAT

	Set-Registry -Path "Registry::HKU\Default_User\SOFTWARE\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" -Name OemPreInstalledAppsEnabled -Value 0
	Set-Registry -Path "Registry::HKU\Default_User\SOFTWARE\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" -Name PreInstalledAppsEnabled -Value 0
	Set-Registry -Path "Registry::HKU\Default_User\SOFTWARE\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" -Name SilentInstalledAppsEnabled -Value 0
	Set-Registry -Path "Registry::HKU\Default_User\SOFTWARE\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" -Name SystemPaneSuggestionsEnabled -Value 0
	Set-Registry -Path "Registry::HKU\Default_User\SOFTWARE\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" -Name ContentDeliveryAllowed -Value 0
	#Set-Registry -Path "Registry::HKU\Default_User\SOFTWARE\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" -Name PreInstalledAppsEverEnabled -Value 0

	# Disable Game Bar (gets rid of popups caused by the xbox app not being installed)
	Write-Output "Disabling Game Bar"
	Set-Registry -Path "Registry::HKU\Default_User\SOFTWARE\Microsoft\Windows\CurrentVersion\GameDVR" -Name AppCaptureEnabled -Value 0
	Set-Registry -Path "Registry::HKU\Default_User\System\GameConfigStore" -Name GameDVR_Enabled -Value 0

	Write-Output "Disabling Bing Search"
	$Search = "Registry::HKU\Default_User\SOFTWARE\Microsoft\Windows\CurrentVersion\Search"
	Set-Registry -Path $Search -Name BingSearchEnabled -Value 0 # ok
	Write-Output "Setting taskbar search to an icon"
	Set-Registry -Path $Search -Name SearchboxTaskbarMode -Value 1
	Set-Registry -Path $Search -Name TraySearchBoxVisible -Value 0

	# (commented out because we disable Feeds system-wide)
	#Write-Output "Disabling Feeds (News) on the taskbar"
	#Set-Registry -Path "Registry::HKU\Default_User\SOFTWARE\Microsoft\Windows\CurrentVersion\Feeds" -Name ShellFeedsTaskbarViewMode -Value 2

	Write-Output "Setting explorer to show file extensions by default."
	Set-Registry -Path "Registry::HKU\Default_User\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name HideFileExt -Value 0
	#Set-Registry -Path "Registry::HKU\Default_User\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name ShowCortanaButton -Value 0

	Write-Output "Disabling automatically installing OneDrive for new users."
	$Run = "Registry::HKU\Default_User\SOFTWARE\Microsoft\Windows\CurrentVersion\Run"
	if (Test-Path $Run) {
		Remove-ItemProperty -Path $Run -Name OneDriveSetup
	}

	reg unload HKU\Default_User
}

function UnpinStart {
	$START_MENU_LAYOUT = @"
<LayoutModificationTemplate xmlns:defaultlayout="http://schemas.microsoft.com/Start/2014/FullDefaultLayout" xmlns:start="http://schemas.microsoft.com/Start/2014/StartLayout" Version="1" xmlns:taskbar="http://schemas.microsoft.com/Start/2014/TaskbarLayout" xmlns="http://schemas.microsoft.com/Start/2014/LayoutModification">
  <LayoutOptions StartTileGroupCellWidth="6" />
  <DefaultLayoutOverride>
	<StartLayoutCollection>
	  <defaultlayout:StartLayout GroupCellWidth="6" />
	</StartLayoutCollection>
  </DefaultLayoutOverride>
  <CustomTaskbarLayoutCollection PinListPlacement="Replace">
	<defaultlayout:TaskbarLayout>
	  <taskbar:TaskbarPinList>
		<taskbar:DesktopApp DesktopApplicationLinkPath="Firefox.lnk" />
		<taskbar:DesktopApp DesktopApplicationLinkPath="%APPDATA%\Microsoft\Windows\Start Menu\Programs\System Tools\File Explorer.lnk" />
	  </taskbar:TaskbarPinList>
	</defaultlayout:TaskbarLayout>
  </CustomTaskbarLayoutCollection>
</LayoutModificationTemplate>
"@
	
	$layoutFile="$env:temp\StartMenuLayout.xml"
	$START_MENU_LAYOUT | Out-File $layoutFile -Encoding ASCII
	Import-StartLayout -LayoutPath $layoutFile -MountPath $env:SystemDrive\
	Remove-Item $layoutFile
}

function InstallFirefox {
	$TempDir = "$env:temp\FirefoxInstall"
	New-Item -ItemType Directory $TempDir
	$FirefoxInstaller = join-path $TempDir firefox_installer.exe
	curl.exe -L "https://download.mozilla.org/?product=firefox-latest&os=win64&lang=en-US" -o "$FirefoxInstaller"
	Start-Process -Wait -FilePath $FirefoxInstaller -ArgumentList "/S"
	Write-Output "Installed Firefox"
	Remove-Item -Path $TempDir -Recurse
}

function InstallExtension {
	param(
		[parameter(Mandatory)] [string]$DownloadUri
	)

	$DownloadUri -match '([^/]+).xpi'
	$Filename = $matches[1]
	$ExtensionPath = "C:\Program Files\Mozilla Firefox\distribution\extensions"

	if (!(Test-Path $ExtensionPath)) {
		New-Item -ItemType Directory $ExtensionPath
	}

	curl.exe -L "$DownloadUri" -o "$ExtensionPath\$Filename.zip"

	$TempDir = "$env:temp\FirefoxExtension"
	New-Item -ItemType Directory $TempDir
	Expand-Archive -Path "$ExtensionPath\$Filename.zip" -DestinationPath $TempDir

	$file = Get-Content "$TempDir\manifest.json" | ConvertFrom-Json
	$extensionId = $file.browser_specific_settings.gecko.id
	Rename-Item -Path $ExtensionPath\$Filename.zip -NewName "$extensionId.xpi"
	Remove-Item -Path $TempDir -Recurse
}

function SetDefaultBrowser {
	$APP_ASSOCIATIONS = @"
<?xml version="1.0" encoding="UTF-8"?>
<DefaultAssociations>
  <Association Identifier=".htm" ProgId="FirefoxHTML-308046B0AF4A39CB" ApplicationName="Firefox" />
  <Association Identifier=".html" ProgId="FirefoxHTML-308046B0AF4A39CB" ApplicationName="Firefox" />
  <Association Identifier=".pdf" ProgId="FirefoxPDF-308046B0AF4A39CB" ApplicationName="Firefox" />
  <Association Identifier=".svg" ProgId="FirefoxURL-308046B0AF4A39CB" ApplicationName="Firefox" />
  <Association Identifier="http" ProgId="FirefoxURL-308046B0AF4A39CB" ApplicationName="Firefox" />
  <Association Identifier="https" ProgId="FirefoxURL-308046B0AF4A39CB" ApplicationName="Firefox" />
</DefaultAssociations>
"@

	$associationsFile="$env:temp\FirefoxDefaultBrowser.xml"
	$APP_ASSOCIATIONS | Out-File $associationsFile -Encoding ASCII
	Dism.exe /Online /Import-DefaultAppAssociations:$associationsFile
	Remove-Item $associationsFile
}

Debloat
Privacy
UnpinStart
InstallFirefox
InstallExtension "https://addons.mozilla.org/firefox/downloads/file/4028976/ublock_origin-1.45.2.xpi"
SetDefaultBrowser
Set-ExecutionPolicy Restricted
